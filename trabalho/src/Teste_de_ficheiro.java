import java.io.*;
import java.util.Scanner;
import java.util.Calendar;


public class Teste_de_ficheiro {
    public static int leituraFicheiroCSV(String[] arraio, int[][] valoresAcumulados, String nomeDoFicheiro) {
        String ficheiro = nomeDoFicheiro;
        String linha = "";
        String titulos = "";
        int cont = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(ficheiro));
            String[] datas = linha.split(",");
            titulos = br.readLine();
            while ((linha = br.readLine()) != null) {
                String[] valores = linha.split(",");
                arraio[cont] = valores[0];
                for (int i = 0; i < 5; i++) {
                    valoresAcumulados[cont][i] = Integer.parseInt(valores[i + 1]);
                }
                cont++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cont;
    }

    public static int verificarOdiaDaSemana(String data) {
        int[] datanova = new int[3];
        int n;
        System.out.println(data);
        String[] dataSeparada = data.split("-");
        datanova[1] = Integer.parseInt(dataSeparada[1]);
        datanova[0] = Integer.parseInt(dataSeparada[0]);
        datanova[2] = Integer.parseInt(dataSeparada[2]);
        Calendar c = Calendar.getInstance();
        c.set(datanova[0], (datanova[1] - 1), datanova[2]);
        int[] days = new int[]{1, 2, 3, 4, 5, 6, 7};
        n = c.get(c.DAY_OF_WEEK);
        return days[n - 1];

    }

    public static int comecaSegunda_AcabarDomingo(String data1, int data, int iniOuFim) {
        int diaDaSemana;
        diaDaSemana = verificarOdiaDaSemana(data1);
        if (iniOuFim == 2) {
            switch (diaDaSemana) {
                case 1:
                    data = data + 1;
                    break;

                case 2:
                    //tá certo
                    break;

                case 3:
                    data = data + 6;
                    break;

                case 4:
                    data = data + 5;
                    break;

                case 5:
                    data = data + 4;
                    break;

                case 6:
                    data = data + 3;
                    break;

                case 7:
                    data = data + 2;
                    break;

            }
        } else {
            switch (diaDaSemana) {
                case 1:
                    //ta certo
                    break;

                case 2:
                    data = data - 1;
                    break;

                case 3:
                    data = data - 2;
                    break;

                case 4:
                    data = data - 3;
                    break;

                case 5:
                    data = data - 4;
                    break;

                case 6:
                    data = data - 5;
                    break;

                case 7:
                    data = data - 6;
                    break;
            }
        }

        return data;
    }


    public static String trocar_A_Ordem(String data1) {
        String[] primeiraDataSeparada = data1.split("-");
        String guardar;
        guardar = primeiraDataSeparada[0];
        primeiraDataSeparada[0] = primeiraDataSeparada[2];
        primeiraDataSeparada[2] = guardar;
        String dataNova = primeiraDataSeparada[0] + "-" + primeiraDataSeparada[1] + "-" + primeiraDataSeparada[2];
        return dataNova;
    }

    public static void trocar_A_Ordem_Do_Ficheiro(String[] FicheiroDasDatas, int cont) {
        for (int i = 0; i < cont; i++) {
            String[] primeiraDataSeparada = FicheiroDasDatas[i].split("-");
            String guardar;
            guardar = primeiraDataSeparada[0];
            primeiraDataSeparada[0] = primeiraDataSeparada[2];
            primeiraDataSeparada[2] = guardar;
            String dataNova = primeiraDataSeparada[0] + "-" + primeiraDataSeparada[1] + "-" + primeiraDataSeparada[2];
            FicheiroDasDatas[i] = dataNova;

        }
    }

    public static int procurarADataNoFicheiro(String data, String[] datasFicheiro, int cont) {
        int guardar = -1;
        for (int lugarNoFicheiro = 0; lugarNoFicheiro < cont; lugarNoFicheiro++) {
            if (datasFicheiro[lugarNoFicheiro].equals(data)) {
                guardar = lugarNoFicheiro;
                break;
            }
        }
        return guardar;
    }

    public static void analiseComparativaTotal(String[] datasFicheiro, int cont, int[][] valores) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Inserir a primeira data do primeiro intervalo de tempo: ");
        String data11 = sc.nextLine();
        System.out.println("Inserir a segunda data do primeiro intervalo de tempo: ");
        String data12 = sc.nextLine();
        System.out.println("Inserir a primeira data do segundo intervalo de tempo: ");
        String data21 = sc.nextLine();
        System.out.println("Inserir a segunda data do segundo intervalo de tempo: ");
        String data22 = sc.nextLine();
        float nDiasMaisPequeno;
        int verificar = 0;
        float media1Intervalo = 0;
        float media2Intervalo = 0;
        float calculoDaDiferença = 0;
        String guardarNomes[] = new String[5];
        guardarNomes[0] = "Não Infetados";
        guardarNomes[1] = "Infetados";
        guardarNomes[2] = "Hospitalizados";
        guardarNomes[3] = "InternadosUCI";
        guardarNomes[4] = "Mortos";
        int verificar2 = 0;
        int escolhaDoFicheiro = 0;
        while (verificar2 == 0) {
            System.out.println("Quer fazer a analise comparativa com o dados totais ou com dados acomulados?");
            System.out.println();
            System.out.println("[1]: Analise com dados Totais;");
            System.out.println("[2]: Analise com dados Acomulados;");
            escolhaDoFicheiro = sc.nextInt();
            switch (escolhaDoFicheiro) {
                case 1:
                    verificar2 = 1;
                    break;

                case 2:
                    verificar2 = 1;
                    trocar_A_Ordem_Do_Ficheiro(datasFicheiro, cont);

                    break;

                default:
                    System.out.println("Valor introduzido incorreto. Por favor, volte a introduzir.");
                    System.out.println();
                    break;
            }
        }
        int valorData11 = procurarADataNoFicheiro(data11, datasFicheiro, cont);
        int valorData12 = procurarADataNoFicheiro(data12, datasFicheiro, cont);
        int valorData21 = procurarADataNoFicheiro(data21, datasFicheiro, cont);
        int valorData22 = procurarADataNoFicheiro(data22, datasFicheiro, cont);
        int nDias1 = valorData12 - valorData11 + 1;
        int nDias2 = valorData22 - valorData21 + 1;
        if (nDias1 > nDias2) {
            nDiasMaisPequeno = nDias2;
        } else {
            nDiasMaisPequeno = nDias1;
        }
        int escolhaDosValores = -1;
        while (verificar == 0) {
            System.out.println("Quer fazer uma análise comparativa de que valores?");
            System.out.println();
            System.out.println("[1]: Numero de Infetados;");
            System.out.println("[2]: Numero de Hospitalizados;");
            System.out.println("[3]: Numero de Internados UCI;");
            System.out.println("[4]: Numero de Mortos;");
            System.out.println("[5]: Todos a cima;");
            escolhaDosValores = sc.nextInt();
            switch (escolhaDosValores) {
                case 1:
                    System.out.println("Análise Comparativa do Numero de infetados");
                    verificar = 1;
                    break;

                case 2:
                    System.out.println("Análise Comparativa do Numero de hospitalizados");
                    verificar = 1;
                    break;

                case 3:
                    System.out.println("Análise Comparativa do Numero de internados UCI");
                    verificar = 1;
                    break;

                case 4:
                    System.out.println("Análise Comparativa do Numero de Mortos");
                    verificar = 1;
                    break;

                case 5:
                    System.out.println("Análise Comparativa de Todos os valores");
                    verificar = 1;
                    break;

                default:
                    System.out.println("Valor introduzido incorreto. Por favor, volte a introduzir.");
                    System.out.println();
            }
        }
        if (escolhaDoFicheiro == 1) {
            if (escolhaDosValores == 1 || escolhaDosValores == 2 || escolhaDosValores == 3 || escolhaDosValores == 4) {
                System.out.println();
                System.out.printf("%-20s%-20s%-5s%-20s%-20s%-5s%-40s", "1º Intervalo", "Dados", "|", "2º intervalo", "Dados", "|", "Diferença dos intervalos de tempo");
                System.out.println();
                for (int i = 0; i < nDiasMaisPequeno; i++) {
                    System.out.printf("%-20s%-20s%-5s%-20s%-20s%-5s%-40s\n", datasFicheiro[valorData11 + i], valores[valorData11 + i][escolhaDosValores], "|", datasFicheiro[valorData21 + i], valores[valorData21 + i][escolhaDosValores], "|", Math.abs((valores[valorData11 + i][escolhaDosValores] - valores[valorData21 + i][escolhaDosValores])));
                    media1Intervalo = media1Intervalo + valores[valorData11 + i][escolhaDosValores];
                    media2Intervalo = media2Intervalo + valores[valorData21 + i][escolhaDosValores];
                }
                float media1 = media1Intervalo / nDiasMaisPequeno;
                float media2 = media2Intervalo / nDiasMaisPequeno;
                System.out.println();
                System.out.print("Média dos dados do 1º intervalo : ");
                System.out.printf("%.4f", (media1));
                System.out.println();
                System.out.print("Média dos dados do 2º intervalo : ");
                System.out.printf("%.4f", (media2));
                System.out.println();
                System.out.print("Média dos dados da diferença dos intervalos : ");
                System.out.printf("%.4f", Math.abs((media1Intervalo - media2Intervalo) / nDiasMaisPequeno));
                System.out.println();
                float desvio1;
                float desvio2;
                float variacao1 = 0;
                float variacao2 = 0;
                for (int v = 0; v < nDiasMaisPequeno; v++) {
                    desvio1 = (valores[valorData11 + v][escolhaDosValores]) - media1;
                    desvio2 = (valores[valorData21 + v][escolhaDosValores]) - media2;

                    variacao1 = variacao1 + Math.abs(desvio1);
                    variacao2 = variacao2 + Math.abs(desvio2);
                }
                double desviopadrao1 = Math.sqrt(variacao1 / nDiasMaisPequeno);
                double desviopadrao2 = Math.sqrt(variacao2 / nDiasMaisPequeno);
                System.out.println();
                System.out.print("Desvio padrão dos dados do 1º intervalo : ");
                System.out.printf("%.4f", desviopadrao1);
                System.out.println();
                System.out.print("Desvio padrão dos dados do 2º intervalo : ");
                System.out.printf("%.4f", desviopadrao2);
                System.out.println();
            } else {
                System.out.println("===========================================================================================================================");
                for (int j = 1; j < 5; j++) {
                    System.out.println("Análise Comparativa do Numero de " + guardarNomes[j]);
                    System.out.printf("%-20s%-20s%-5s%-20s%-20s%-5s%-40s", "1º Intervalo", "Dados", "|", "2º intervalo", "Dados", "|", "Diferença dos intervalos de tempo");
                    System.out.println();
                    for (int i = 0; i < nDiasMaisPequeno; i++) {
                        System.out.printf("%-20s%-20s%-5s%-20s%-20s%-5s%-40s\n", datasFicheiro[valorData11 + i], valores[valorData11 + i][j], "|", datasFicheiro[valorData21 + i], valores[valorData21 + i][j], "|", Math.abs((valores[valorData11 + i][j] - valores[valorData21 + i][j])));
                        media1Intervalo = media1Intervalo + valores[valorData11 + i][j];
                        media2Intervalo = media2Intervalo + valores[valorData21 + i][j];
                    }
                    float media1 = media1Intervalo / nDiasMaisPequeno;
                    float media2 = media2Intervalo / nDiasMaisPequeno;
                    System.out.println();
                    System.out.print("Média dos dados do 1º intervalo : ");
                    System.out.printf("%.4f", (media1Intervalo / nDiasMaisPequeno));
                    System.out.println();
                    System.out.print("Média dos dados do 2º intervalo : ");
                    System.out.printf("%.4f", (media2Intervalo / nDiasMaisPequeno));
                    System.out.println();
                    System.out.print("Média dos dados da diferença dos intervalos : ");
                    System.out.printf("%.4f", Math.abs((media1Intervalo - media2Intervalo) / nDiasMaisPequeno));
                    System.out.println();
                    float desvio1;
                    float desvio2;
                    float variacao1 = 0;
                    float variacao2 = 0;
                    for (int v = 0; v < nDiasMaisPequeno; v++) {
                        desvio1 = (valores[valorData11 + v][j] - valores[(valorData11 + v) - 1][j]) - media1;
                        desvio2 = (valores[valorData21 + v][j] - valores[(valorData21 + v) - 1][j]) - media2;
                        variacao1 = variacao1 + Math.abs(desvio1);
                        variacao2 = variacao2 + Math.abs(desvio2);
                    }
                    double desviopadrao1 = Math.sqrt(variacao1 / nDiasMaisPequeno);
                    double desviopadrao2 = Math.sqrt(variacao2 / nDiasMaisPequeno);
                    System.out.println();
                    System.out.print("Desvio padrão dos dados do 1º intervalo : ");
                    System.out.printf("%.4f", desviopadrao1);
                    System.out.println();
                    System.out.print("Desvio padrão dos dados do 2º intervalo : ");
                    System.out.printf("%.4f", desviopadrao2);
                    System.out.println();
                    System.out.println();
                    System.out.println();
                    System.out.println("===========================================================================================================================");
                    media1Intervalo = 0;
                    media2Intervalo = 0;
                }
            }
        } else {
            if (valorData11 == 0) {
                System.out.println("Não existem dados no ficheiro csv para calcular a análise no dia " + datasFicheiro[0]);
                valorData11 = 1;
                if (nDias1 <= nDias2) {
                    nDiasMaisPequeno = nDiasMaisPequeno - 1;
                }
            } else if (valorData21 == 0) {
                System.out.println("Não existem dados no ficheiro csv para calcular a análise no dia " + datasFicheiro[0]);
                valorData21 = 1;
                if (nDias2 <= nDias1) {
                    nDiasMaisPequeno = nDiasMaisPequeno - 1;
                }
            }
            if (escolhaDosValores == 1 || escolhaDosValores == 2 || escolhaDosValores == 3 || escolhaDosValores == 4) {
                System.out.println();
                System.out.printf("%-20s%-20s%-5s%-20s%-20s%-5s%-40s", "1º Intervalo", "Dados", "|", "2º intervalo", "Dados", "|", "Diferença dos intervalos de tempo");
                System.out.println();
                for (int i = 0; i < nDiasMaisPequeno; i++) {
                    calculoDaDiferença = (valores[valorData11 + i][escolhaDosValores] - valores[(valorData11 + i) - 1][escolhaDosValores]) - (valores[valorData21 + i][escolhaDosValores] - valores[(valorData21 + i) - 1][escolhaDosValores]);
                    System.out.printf("%-20s%-20s%-5s%-20s%-20s%-5s%-40s\n", datasFicheiro[valorData11 + i], (valores[valorData11 + i][escolhaDosValores] - valores[(valorData11 + i) - 1][escolhaDosValores]), "|", datasFicheiro[valorData21 + i], (valores[valorData21 + i][escolhaDosValores] - valores[(valorData21 + i) - 1][escolhaDosValores]), "|", Math.abs(calculoDaDiferença));
                    media1Intervalo = media1Intervalo + valores[valorData11 + i][escolhaDosValores] - valores[(valorData11 + i) - 1][escolhaDosValores];
                    media2Intervalo = media2Intervalo + valores[valorData21 + i][escolhaDosValores] - valores[(valorData21 + i) - 1][escolhaDosValores];
                }
                float media1 = media1Intervalo / nDiasMaisPequeno;
                float media2 = media2Intervalo / nDiasMaisPequeno;
                System.out.println();
                System.out.print("Média dos dados do 1º intervalo : ");
                System.out.printf("%.4f", (media1));
                System.out.println();
                System.out.print("Média dos dados do 2º intervalo : ");
                System.out.printf("%.4f", (media2));
                System.out.println();
                System.out.print("Média dos dados da diferença dos intervalos : ");
                System.out.printf("%.4f", Math.abs((media1Intervalo - media2Intervalo) / nDiasMaisPequeno));
                System.out.println();
                float desvio1;
                float desvio2;
                float variacao1 = 0;
                float variacao2 = 0;
                for (int v = 0; v < nDiasMaisPequeno; v++) {
                    desvio1 = (valores[valorData11 + v][escolhaDosValores] - valores[(valorData11 + v) - 1][escolhaDosValores]) - media1;
                    desvio2 = (valores[valorData21 + v][escolhaDosValores] - valores[(valorData21 + v) - 1][escolhaDosValores]) - media2;
                    variacao1 = variacao1 + Math.abs(desvio1);
                    variacao2 = variacao2 + Math.abs(desvio2);
                }
                double desviopadrao1 = Math.sqrt(variacao1 / nDiasMaisPequeno);
                double desviopadrao2 = Math.sqrt(variacao2 / nDiasMaisPequeno);
                System.out.println();
                System.out.print("Desvio padrão dos dados do 1º intervalo : ");
                System.out.printf("%.4f", desviopadrao1);
                System.out.println();
                System.out.print("Desvio padrão dos dados do 2º intervalo : ");
                System.out.printf("%.4f", desviopadrao2);
                System.out.println();
            } else {
                System.out.println("===========================================================================================================================");
                for (int j = 1; j < 5; j++) {
                    System.out.println("Análise Comparativa do Numero de " + guardarNomes[j]);
                    System.out.printf("%-20s%-20s%-5s%-20s%-20s%-5s%-40s", "1º Intervalo", "Dados", "|", "2º intervalo", "Dados", "|", "Diferença dos intervalos de tempo");
                    System.out.println();
                    for (int i = 0; i < nDiasMaisPequeno; i++) {
                        calculoDaDiferença = (valores[valorData11 + i][j] - valores[(valorData11 + i) - 1][j]) - (valores[valorData21 + i][j] - valores[(valorData21 + i) - 1][j]);
                        System.out.printf("%-20s%-20s%-5s%-20s%-20s%-5s%-40s\n", datasFicheiro[valorData11 + i], (valores[valorData11 + i][j] - valores[(valorData11 + i) - 1][j]), "|", datasFicheiro[valorData21 + i], (valores[valorData21 + i][j] - valores[(valorData21 + i) - 1][j]), "|", Math.abs((calculoDaDiferença)));
                        media1Intervalo = media1Intervalo + valores[valorData11 + i][j] - valores[(valorData11 + i) - 1][j];
                        media2Intervalo = media2Intervalo + valores[valorData21 + i][j] - valores[(valorData21 + i) - 1][j];
                    }
                    float media1 = media1Intervalo / nDiasMaisPequeno;
                    float media2 = media2Intervalo / nDiasMaisPequeno;
                    System.out.println();
                    System.out.print("Média dos dados do 1º intervalo : ");
                    System.out.printf("%.4f", (media1));
                    System.out.println();
                    System.out.print("Média dos dados do 2º intervalo : ");
                    System.out.printf("%.4f", (media2));
                    System.out.println();
                    System.out.print("Média dos dados da diferença dos intervalos : ");
                    System.out.printf("%.4f", Math.abs((media1Intervalo - media2Intervalo) / nDiasMaisPequeno));
                    System.out.println();
                    System.out.println();
                    float desvio1;
                    float desvio2;
                    float variacao1 = 0;
                    float variacao2 = 0;
                    for (int v = 0; v < nDiasMaisPequeno; v++) {
                        desvio1 = (valores[valorData11 + v][j]) - media1;
                        desvio2 = (valores[valorData21 + v][j]) - media2;

                        variacao1 = variacao1 + Math.abs(desvio1);
                        variacao2 = variacao2 + Math.abs(desvio2);
                    }
                    double desviopadrao1 = Math.sqrt(variacao1 / nDiasMaisPequeno);
                    double desviopadrao2 = Math.sqrt(variacao2 / nDiasMaisPequeno);
                    System.out.print("Desvio padrão dos dados do 1º intervalo : ");
                    System.out.printf("%.4f", desviopadrao1);
                    System.out.println();
                    System.out.print("Desvio padrão dos dados do 2º intervalo : ");
                    System.out.printf("%.4f", desviopadrao2);
                    System.out.println();
                    System.out.println("===========================================================================================================================");
                }
            }

        }
    }


    public static void procurarAData(String data1, String data2, String[] datasFicheiro, int cont, int X, int[][] valores_Acumulados, int escolha) {
        for (int DataIni = 0; DataIni < cont; DataIni++) {
            if (datasFicheiro[DataIni].equals(data1)) {
                for (int DataFim = DataIni; DataFim <= cont; DataFim++) {
                    if (datasFicheiro[DataFim].equals(data2)) {
                        if (escolha == 0) {
                            switch (X) {
                                case 0:
                                    calculoDiarioAcumulado(DataIni, DataFim, valores_Acumulados, datasFicheiro);
                                    //dia
                                    break;

                                case 1:
                                    int segunda = 2;
                                    int domingo = 1;
                                    DataIni = comecaSegunda_AcabarDomingo(data1, DataIni, segunda);
                                    DataFim = comecaSegunda_AcabarDomingo(data2, DataFim, domingo);
                                    calculoSemanal(DataIni, DataFim, valores_Acumulados, datasFicheiro);
                                    //semanal
                                    break;

                                case 2:
                                    int mostrar = comecarNumMesInteiro(data1, DataIni);
                                    System.out.println(datasFicheiro[mostrar]);
                                    int mostrar2 = acabarNoDia30(data2, DataFim);
                                    System.out.println(datasFicheiro[mostrar2]);
                                    calculoMensalAcumulado(valores_Acumulados, mostrar, mostrar2, datasFicheiro, cont);
                                    break;
                            }
                        }
                        if (escolha == 1) {
                            switch (X) {
                                case 0:
                                    calculoDiarioTotal(DataIni, DataFim, valores_Acumulados, datasFicheiro);
                                    break;
                                //dia

                                case 1:
                                    data2 = trocar_A_Ordem(data2);
                                    data1 = trocar_A_Ordem(data1);
                                    System.out.println("teste1");
                                    int segunda = 2;
                                    int domingo = 1;
                                    DataIni = comecaSegunda_AcabarDomingo(data1, DataIni, segunda);
                                    DataFim = comecaSegunda_AcabarDomingo(data2, DataFim, domingo);
                                    calculoSemanalTotal(DataIni, DataFim, valores_Acumulados, datasFicheiro);
                                    break;
                                //semanal

                                case 2:
                                    System.out.println();
                                    int mostrar = comecarNumMesInteiro(data1, DataIni);
                                    int mostrar2 = acabarNoDia30(data2, DataFim);
                                    calculos_mensal_Total(valores_Acumulados, mostrar, mostrar2, datasFicheiro);


                            }
                        }
                        break;
                    }
                }
            }
        }
    }

    public static void calculoMensalAcumulado(int[][] valores_Acumulados, int DataIni, int DataFim, String[] datas, int cont) {
        String[] dataSeparada = datas[DataIni].split("-");
        DataFim++;
        int contador2 = 0;
        int mesprimeiro = Integer.parseInt(dataSeparada[1]);
        int guardar = mesprimeiro;
        int guardarPrimeiroDia = DataIni;
        int mes = 0;
        for (int j = DataIni; j <= DataFim; j++) {
            contador2++;
            if (cont != contador2) {
                String[] dataSeparadaMeio = datas[j].split("-");
                mes = Integer.parseInt(dataSeparadaMeio[1]);
            } else {
                mes = 13;
                j = j + 1;
            }

            if (mes != guardar) {
                System.out.println("analise mensal do dia : " + datas[guardarPrimeiroDia] + " até dia " + datas[j - 1]);
                System.out.println("Novos casos diarios num mês: " + (valores_Acumulados[j - 1][1] - valores_Acumulados[guardarPrimeiroDia][1]));
                System.out.println("Novos hospitalizados num mês: " + (valores_Acumulados[j - 1][2] - valores_Acumulados[guardarPrimeiroDia][2]));
                System.out.println("Novos internados UCI num mês: " + (valores_Acumulados[j - 1][3] - valores_Acumulados[guardarPrimeiroDia][3]));
                System.out.println("Novos mortos num mês: " + (valores_Acumulados[j - 1][4] - valores_Acumulados[guardarPrimeiroDia][4]));
                System.out.println();
                guardarPrimeiroDia = j;
                guardar = mes;
            }
        }
    }

    public static int comecarNumMesInteiro(String data1, int DataIni) {
        String[] dataSeparada = data1.split("-");
        int dia = Integer.parseInt(dataSeparada[0]);
        int mes = Integer.parseInt(dataSeparada[1]);
        int ano = Integer.parseInt(dataSeparada[2]);
        System.out.println("check 1");
        if (dia != 1) {
            if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
                System.out.println("check 2");
                //meses com 30 dias
                while (dia <= 30) {
                    dia++;
                    DataIni++;
                }
            } else if (mes == 2) {
                if (ano == 2020) {
                    while (dia <= 29) {
                        dia++;
                        DataIni++;
                    }
                } else {
                    while (dia <= 28) {
                        dia++;
                        DataIni++;
                    }
                }
            } else {
                while (dia <= 31) {
                    dia++;
                    DataIni++;
                }
                if (mes == 12) {
                }
            }
        }
        return DataIni;
    }

    public static int acabarNoDia30(String data2, int DataFim) {
        String[] dataSeparada = data2.split("-");
        int dia = Integer.parseInt(dataSeparada[0]);
        int mes = Integer.parseInt(dataSeparada[1]);
        int ano = Integer.parseInt(dataSeparada[2]);

        int verificar = 0;
        if (mes == 4 || mes == 6 || mes == 9 || mes == 11) {
            if (dia == 30) {
                verificar = 1;
            }
        } else if (mes == 2) {
            if (ano == 2020) {
                if (dia == 29) {
                    verificar = 1;
                }
            } else {
                if (dia == 28) {
                    verificar = 1;
                }
            }
        } else {
            if (dia == 31) {
                verificar = 1;
            }
        }
        if (verificar == 0) {
            while (dia >= 1) {
                dia--;
                DataFim--;
            }
        }
        return DataFim;
    }

    public static void calculos_mensal_Total(int[][] valores_Acumulados, int DataIni, int DataFim, String[] datas) {
        DataFim = DataFim + 1;
        String[] dataSeparada = datas[DataIni].split("-");
        int mesprimeiro = Integer.parseInt(dataSeparada[1]);
        int guardar = mesprimeiro;
        int guardarPrimeiroDia = DataIni;
        int mes;
        int infetados = 0;
        int hospitalizados = 0;
        int internadosUCI = 0;
        int obitos = 0;
        for (int j = DataIni; j <= DataFim; j++) {
            String[] dataSepMeio = datas[j].split("-");
            mes = Integer.parseInt(dataSepMeio[1]);
            if (mes == guardar) {
                infetados = infetados + valores_Acumulados[j][1];
                hospitalizados = hospitalizados + valores_Acumulados[j][2];
                internadosUCI = internadosUCI + valores_Acumulados[j][3];
                obitos = obitos + valores_Acumulados[j][4];
            } else {
                System.out.println("analise mensal do dia : " + datas[guardarPrimeiroDia] + " até dia " + datas[j - 1]);
                System.out.println("analise mensal de infetadsos : " + infetados);
                System.out.println("analise mensal de hospitalizados :  " + hospitalizados);
                System.out.println("analise mensal de internadosUCI : " + internadosUCI);
                System.out.println("analise mensal de obitos : " + obitos);
                System.out.println();
                guardarPrimeiroDia = j;
                guardar = mes;
                infetados = 0;
                hospitalizados = 0;
                internadosUCI = 0;
                obitos = 0;
            }
        }
    }

    public static void calculoDiarioAcumulado(int DataIni, int DataFim, int[][] valores_Acumulados, String[] datas) {
        for (int j = DataIni; j <= DataFim; j++) {
            if (j != 0) {
                System.out.println("Análise do dia " + (datas[j]));
                System.out.println("Novos casos diarios: " + (valores_Acumulados[j][1] - valores_Acumulados[j - 1][1]));
                System.out.println("Novos hospitalizados: " + (valores_Acumulados[j][2] - valores_Acumulados[j - 1][2]));
                System.out.println("Novos internados UCI: " + (valores_Acumulados[j][3] - valores_Acumulados[j - 1][3]));
                System.out.println("Novos mortos: " + (valores_Acumulados[j][4] - valores_Acumulados[j - 1][4]));
                System.out.println();
            } else {
                System.out.println("Não há dados para a primeira data");
            }
        }
    }

    public static void calculoSemanal(int DataIni, int DataFim, int[][] valores_Acumulados, String[] datas) {
        for (int i = DataIni; i <= DataFim; i = i + 7) {
            if (i + 6 <= DataFim) {
                System.out.println("Análise semanal de " + (datas[i]) + " até " + (datas[i + 6]));  //antes tinha trocar as datas
                System.out.println("Novos casos diarios: " + (valores_Acumulados[i + 6][1] - valores_Acumulados[i][1]));
                System.out.println("Novos hospitalizados: " + (valores_Acumulados[i + 6][2] - valores_Acumulados[i][2]));
                System.out.println("Novos internados UCI: " + (valores_Acumulados[i + 6][3] - valores_Acumulados[i][3]));
                System.out.println("Novos mortos: " + (valores_Acumulados[i + 6][4] - valores_Acumulados[i][4]));
                System.out.println();
            }
        }
    }

    public static void calculoDiarioTotal(int DataIni, int DataFim, int[][] valores_Acumulados, String[] datas) {
        for (int j = DataIni; j <= DataFim; j++) {
            System.out.println("Análise do dia " + (datas[j]));
            System.out.println("Numero de infetados: " + (valores_Acumulados[j][0]));
            System.out.println("Numero de não infetados: " + (valores_Acumulados[j][1]));
            System.out.println("Numero de  hospitalizados: " + (valores_Acumulados[j][2]));
            System.out.println("Numero de internados UCI: " + (valores_Acumulados[j][3]));
            System.out.println("Novos mortos: " + (valores_Acumulados[j][4]));
            System.out.println();
        }
    }

    public static void calculoSemanalTotal(int DataIni, int DataFim, int[][] valores_Acumulados, String[] datas) {
        int nInfetados = 0;
        int nHospitalizados = 0;
        int nUCI = 0;
        int nMortos = 0;
        int x;
        int i = DataIni;
        if ((DataFim - 6 >= DataIni)) {
            for (int j = DataIni; j + 6 <= DataFim; j = j + 7) {
                x = i;
                while (i < (x + 7)) { //ver se não passa uma vez a mais
                    nInfetados = nInfetados + valores_Acumulados[i][1];
                    nHospitalizados = nHospitalizados + valores_Acumulados[i][2];
                    nUCI = nUCI + valores_Acumulados[i][3];
                    nMortos = nMortos + valores_Acumulados[i][4];
                    i++;
                }
                System.out.println("Análise semanal do dia  " + (datas[j]) + " até ao dia " + (datas[j + 6]));
                System.out.println("Numero de infetados: " + nInfetados);
                System.out.println("Numero de  hospitalizados: " + nHospitalizados);
                System.out.println("Numero de internados UCI: " + nUCI);
                System.out.println("Novos mortos: " + nMortos);
                System.out.println();
                nInfetados = 0;
                nHospitalizados = 0;
                nUCI = 0;
                nMortos = 0;
            }
        } else {
            System.out.println("O intervalo de tempo, " + datas[DataIni] + " a " + datas[DataFim] + ", é muito pequeno "); //preciso que alguem veja como escrever isto melhor
        }
    }

    public static double[][] probabilidadestxt() throws FileNotFoundException {
        Scanner scan = new Scanner(System.in);
        String fileloc = "";
        System.out.println("<< Escolha a matriz a utilizar >>");
        System.out.println("[1]     Matriz de transição A");
        System.out.println("[2]     Matriz de transição B");
        System.out.print(": ");
        int opc = scan.nextInt();
        switch (opc) {
            case 1:
                fileloc = "exemploMatrizTransicoes.txt";
                break;
            case 2:
                fileloc = "matrizTransicoesB.txt";
                break;
        }
        File file = new File(fileloc);
        Scanner bReader = new Scanner(file);
        double[][] matMarkov = new double[5][5];
        String s;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (bReader.hasNextLine()) {
                    if (!(s = bReader.nextLine()).isBlank()) {
                        String[] test = s.split("=");
                        matMarkov[i][j] = Double.parseDouble(test[1]);
                    } else {
                        s = bReader.nextLine();
                        String[] test = s.split("=");
                        matMarkov[i][j] = Double.parseDouble(test[1]);
                    }
                }
            }
        }
        return matMarkov;
    }

    public static double[] previsão(double[][] matmark) throws FileNotFoundException {
        double[][] mli = new double[4][4];
        double[][] mt1 = new double[4][4];
        double[][] mt2 = new double[4][4];
        double[][] MatrizInversa = new double[4][4];
        double[][] ml = new double[4][4];
        double[][] mu = new double[4][4];
        double[][] mui = new double[4][4];
        double[][] ident = new double[4][4];
        double[] constante = new double[4];
        double[] result = new double[4];
        mt1 = matmark;
        for (int i = 0; i < 4; i++) {
            constante[i] = 1;
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (i == j) {
                    ident[i][j] = 1;
                    mu[i][j] = 1;
                    mui[i][j] = 1;
                } else
                    ident[i][j] = 0;
            }
        }
        for (int g = 0; g < 4; g++) {
            for (int j = 0; j < 4; j++) {
                mt2[g][j] = ident[g][j] - mt1[g][j];
            }
        }
        ml[0][0] = mt2[0][0];
        ml[1][0] = mt2[1][0];
        ml[2][0] = mt2[2][0];
        ml[3][0] = mt2[3][0];
        mu[0][1] = (mt2[0][1] / mt2[0][0]);
        mu[0][2] = (mt2[0][2] / mt2[0][0]);
        mu[0][3] = (mt2[0][3] / mt2[0][0]);
        ml[1][1] = (mt2[1][1] - (ml[1][0] * mu[0][1]));
        ml[2][1] = (mt2[2][1] - (ml[2][0] * mu[0][1]));
        ml[3][1] = (mt2[3][1] - (ml[3][0]) * mu[0][1]);
        mu[1][2] = ((mt2[1][2] - (ml[1][0]) * mu[0][2]) / ml[1][1]);
        mu[1][3] = ((mt2[1][3] - (ml[1][0]) * mu[0][3]) / ml[1][1]);
        ml[2][2] = (mt2[2][2] - (ml[2][0]) * mu[0][2] - ml[2][1] * mu[2][1]);
        mu[2][3] = ((mt2[2][3] - ml[2][0] * mu[0][3] - ml[2][1] * mu[1][3]) / ml[2][2]);
        ml[3][2] = (mt2[3][2] - ml[3][0] * mu[0][2] - ml[3][1] * mu[1][2]);
        ml[3][3] = (mt2[3][3] - ml[3][0] * mu[0][3] - ml[3][1] * mu[1][3] - ml[3][2] * mu[3][3]);
        mli[0][0] = (1 / ml[0][0]);
        mli[1][1] = (1 / ml[1][1]);
        mli[2][2] = (1 / ml[2][2]);  //cuidado
        mli[1][0] = ((-ml[1][0] * mli[0][0]) / ml[1][1]);
        mli[2][0] = (((-ml[2][0] * mli[0][0]) - (ml[2][1] * mli[1][0])) / ml[2][2]);
        mli[3][0] = (((-ml[3][0] * mli[0][0]) - (ml[3][1] * mli[1][0]) - (ml[3][0] * mli[0][0])));//cuidado
        mui[0][1] = -mu[0][1];
        mui[1][2] = -mu[1][2];
        mui[2][3] = -mu[2][3];
        mui[0][2] = ((-mu[0][1] * mui[1][2]) - mu[0][2]);
        mui[1][3] = ((-mu[1][2] * mui[2][3]) - mu[1][3]);
        mui[0][3] = ((-mu[0][1] * mui[1][3]) - (mu[0][2] * mui[2][3]) - mu[0][3]);//cuidado
        for (int c = 0; c < 4; c++) {
            for (int j = 0; j < 4; j++) {
                for (int m = 0; m < 4; m++) {
                    MatrizInversa[c][j] += mui[c][m] * mli[m][j];
                }
            }
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                result[i] += constante[j] * MatrizInversa[i][j];
            }
        }
        return result;
    }

    public static void diasatemorrer(double[][] matmark) throws FileNotFoundException {
        double[] result = previsão(matmark);
        System.out.print("Não infetados: ");
        System.out.printf("%.2f\n", result[0]);
        System.out.print("Infetados: ");
        System.out.printf("%.2f\n", result[1]);
        System.out.print("Hospitalizados: ");
        System.out.printf("%.2f\n", result[2]);
        System.out.print("UCI: ");
        System.out.printf("%.2f\n", result[3]);
    }

    public static void previsaomMark() throws FileNotFoundException {
        int[][] valores_Acumulados = new int[10000][5];
        String[] datasDoFicheiro = new String[100000];
        String linha = "";
        int cont = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader("totalPorEstadoCovid19EmCadaDia.csv"));
            br.readLine(); // para tirar o rotulo da informação
            while ((linha = br.readLine()) != null) {
                String[] valores = linha.split(",");
                datasDoFicheiro[cont] = valores[0];
                for (int i = 0; i < 5; i++) {
                    valores_Acumulados[cont][i] = Integer.parseInt(valores[i + 1]);
                }
                cont++;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scanner scan = new Scanner(System.in);
        int ultData = 0;
        double[] prev = new double[5];
        double[] calcI = new double[5];
        double[] vetDadosD = new double[5];
        double[][] mark = probabilidadestxt();
        double[][] elev1 = new double[5][5];
        double[][] elevf = new double[5][5];
        double sum = 0;
        double sum2 = 0;
        System.out.println("=========== Previsao de dados ===========");
        System.out.print("Insira a data para prever: ");
        String dataprev = scan.nextLine();
        String[] data = dataprev.split("-");
        int elevado = Integer.parseInt(data[0]);
        int ret1 = elevado;
        System.out.print("Insira o dia que pretende usar os dados: ");
        String datai = scan.nextLine();
        String[] dataf = datai.split("-");
        int f = Integer.parseInt(dataf[0]);
        int ret2 = f;
        for (int dataIni = 0; dataIni < cont; dataIni++) {
            if (datasDoFicheiro[dataIni].equals(datai)) {
                ultData = dataIni;
                break;
            }
        }
        System.out.println(ultData);
        for (int j = 0; j < 5; j++) {
            vetDadosD[j] = valores_Acumulados[ultData][j];

        }

        for (int k = 0; k < 5; k++) {
            for (int h = 0; h < 5; h++) {
                elevf[k][h] = mark[k][h];
            }
        }

        if (ret1 > ret2) {
            elevado = ret1 - ret2;
        }
        for (int c = 1; c < elevado; c++) {
            for (int p = 0; p < 5; p++) {
                for (int k = 0; k < 5; k++) {
                    for (int h = 0; h < 5; h++) {
                        sum = sum + (elevf[p][h] * mark[h][k]);
                    }
                    elev1[p][k] = sum;
                    sum = 0;
                }
            }
            for (int k = 0; k < 5; k++) {
                for (int h = 0; h < 5; h++) {
                    elevf[k][h] = elev1[k][h];
                }
            }
        }

        for (int k = 0; k < 5; k++) {
            for (int h = 0; h < 5; h++) {
                sum2 = sum2 + (elevf[k][h] * vetDadosD[h]);
            }
            prev[k] = sum2;
            sum2 = 0;
        }
        System.out.println("============================");
        System.out.print("Não infetados: ");
        System.out.printf("%.2f\n", prev[0]);
        System.out.print("Infetados: ");
        System.out.printf("%.2f\n", prev[1]);
        System.out.print("Hospitalizados: ");
        System.out.printf("%2.2f\n", prev[2]);
        System.out.print("UCI: ");
        System.out.printf("%.2f\n", prev[3]);
        System.out.print("Obitos: ");
        System.out.printf("%.2f\n", prev[4]);
        System.out.println("============================");
    }

    public static void inserirficheiro(String nomeDoFicheiro, String[] datasDoFicheiro, int[][] valores_Acumulados) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Escreva a localização do novo ficheiro csv:");
        nomeDoFicheiro = sc.next();
        int contador = leituraFicheiroCSV(datasDoFicheiro, valores_Acumulados, nomeDoFicheiro);
    }

    public static void analiseData(String nomeDoFicheiro, String[] datasDoFicheiro, int[][] valores_Acumulados, int escolha) {
        Scanner sc = new Scanner(System.in);
        System.out.println("==================== MSP ====================");
        System.out.print("Inserir Data Inicial");
        System.out.format("%15s", "(dd-mm-aaaa): ");
        String data1 = sc.nextLine();
        System.out.println();
        System.out.print("Inserir Data Final");
        System.out.format("%17s", "(dd-mm-aaaa): ");
        String data2 = sc.nextLine();
        System.out.println();
        int teste = 1;
        int X = -1;
        while (teste != 0) {
            System.out.println("Escolha o regime temporal de analise:");
            System.out.print("Diario");
            System.out.format("%16s", "[0]");
            System.out.println();
            System.out.print("Semanal");
            System.out.format("%15s", "[1]");
            System.out.println();
            System.out.print("Mensal");
            System.out.format("%16s", "[2]");
            System.out.println();
            System.out.print("Opção: ");
            X = sc.nextInt();
            System.out.println();
            if (X == 0 || X == 1 || X == 2) {
                teste = 0;
            }
        }

        int contador = leituraFicheiroCSV(datasDoFicheiro, valores_Acumulados, nomeDoFicheiro);
        if (escolha == 0) {
            trocar_A_Ordem_Do_Ficheiro(datasDoFicheiro, contador);

        }
        System.out.println("=============================================");
        procurarAData(data1, data2, datasDoFicheiro, contador, X, valores_Acumulados, escolha);
    }
    public static void impfich(int escolha1,int cont, String[] datas, int[][] valoresAcumulados){
        if (escolha1 == 1) {
            for (int x = 0; x < cont; x++) {
                System.out.printf("%-15s", datas[x]);
                for (int i = 0; i < 5; i++) {
                    System.out.printf("%-10d", valoresAcumulados[x][i]);
                    System.out.print(" ");
                }
                System.out.println();
            }
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(System.in);
        String nomeDoFicheiro = "";
        int valor_gigante = 10000;  //10000 dias = 27 anos
        String[] datasDoFicheiro = new String[valor_gigante];
        int[][] valores_Acumulados = new int[valor_gigante][5];
        double matmark[][] = new double[5][5];
        int sair = 1;
        int esc;
        while (sair == 1) {
            int teste2 = 0;
            int escolha = -1;
            while (teste2 == 0) {
                System.out.println("===== Escolha que ficheiro usar  =====");
                System.out.println();
                System.out.print("Dados Acomulados            [0]");
                System.out.println();
                System.out.print("Dados Totais                [1]");
                System.out.println();
                System.out.println("introduzir novo ficheiro    [2]");
                System.out.println();
                System.out.print("Opção: ");
                escolha = sc.nextInt();
                System.out.println("======================================");
                switch (escolha) {
                    case 0:
                        nomeDoFicheiro = "exemplo.csv";
                        teste2 = 1;
                        break;

                    case 1:
                        nomeDoFicheiro = "totalPorEstadoCovid19EmCadaDia.csv";
                        teste2 = 1;
                        break;
                    case 2:
                        inserirficheiro(nomeDoFicheiro, datasDoFicheiro, valores_Acumulados);
                        break;
                    default:
                        System.out.println("Dado incorreto;");
                        System.out.println();
                        break;
                }
            }
            int opc;
            int contador = leituraFicheiroCSV(datasDoFicheiro, valores_Acumulados, nomeDoFicheiro);
            int escolha1 = -1;
            int teste1 = 0;
            while (teste1 ==0) {
                System.out.println("Pretende visualizar o ficheiro?");
                System.out.println("[1]      Sim");
                System.out.println("[2]      Nao");
                escolha1 = sc.nextInt();
                switch (escolha1) {
                    case 1:
                        impfich(escolha1, contador, datasDoFicheiro, valores_Acumulados);
                        teste1 =1;
                        break;
                    case 2:
                        teste1 =1;
                        break;
                    default:
                        System.out.println("Voltar a inserir");
                }
            }
            System.out.println("");
            System.out.println("<<<<< <<<<<   MENU   >>>>> >>>>>");
            System.out.println("  [1]    Análise de dados");
            System.out.println("  [2]    Previsão Evolução Pandemia");
            System.out.println("  [3]    Previsão de dias ate um individuo morrer");
            System.out.println("  [4]    Análise Comparativa");
            System.out.println("  [5]    Fechar Programa");
            System.out.println("");
            System.out.print("escolha qual a opção: ");
            opc = sc.nextInt();
            switch (opc) {
                case 1:
                    analiseData(nomeDoFicheiro, datasDoFicheiro, valores_Acumulados, escolha);
                    System.out.println();
                    System.out.println("<<  Deseja sair? >>");
                    System.out.println("(0) Sim    (1) Não");
                    System.out.print(": ");
                    esc = sc.nextInt();
                    System.out.println("<< << <<  >> >> >>");
                    switch (esc) {
                        case 0:
                            sair = 0;
                            break;
                        case 1:
                            break;
                    }
                    break;
                case 2:
                    previsaomMark();
                    System.out.println();
                    System.out.println("<<  Deseja sair? >>");
                    System.out.println("(0) Sim    (1) Não");
                    System.out.print(": ");
                    esc = sc.nextInt();
                    System.out.println("<< << <<  >> >> >>");
                    switch (esc) {
                        case 0:
                            sair = 0;
                            break;
                        case 1:
                            break;
                    }
                    break;
                case 3:
                    matmark = probabilidadestxt();
                    diasatemorrer(matmark);
                    System.out.println();
                    System.out.println("<<  Deseja sair? >>");
                    System.out.println("(0) Sim    (1) Não");
                    System.out.print(": ");
                    esc = sc.nextInt();
                    System.out.println("<< << <<  >> >> >>");
                    switch (esc) {
                        case 0:
                            sair = 0;
                            break;
                        case 1:
                            break;
                    }
                    break;
                case 4:
                    analiseComparativaTotal(datasDoFicheiro, contador, valores_Acumulados);
                    System.out.println();
                    System.out.println("<<  Deseja sair? >>");
                    System.out.println("(0) Sim    (1) Não");
                    System.out.print(": ");
                    esc = sc.nextInt();
                    System.out.println("<< << <<  >> >> >>");
                    switch (esc) {
                        case 0:
                            sair = 0;
                            break;
                        case 1:
                            break;
                    }
                    break;
                case 5:
                    System.out.println();
                    System.out.println("<< Deseja sair? >>");
                    System.out.println("[0]       Sim");
                    System.out.println("[1]       Não");
                    System.out.println();
                    System.out.print(": ");
                    esc = sc.nextInt();
                    System.out.println("<< << <<  >> >> >>");
                    switch (esc) {
                        case 0:
                            sair = 0;
                            break;
                        case 1:
                            break;
                    }
                    break;
            }
        }
    }
}